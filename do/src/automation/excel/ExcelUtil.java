package automation.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

///  ASCII Code


public class ExcelUtil {
	
	
	public static Map<String, String> DataMap;
	public static void main(String[] args) throws IOException {
		Map<String, String> hmObj=new HashMap<String, String>();
		hmObj.put("MyName", "Rahul");
		hmObj.put("Address", "Mondh");
		hmObj.put("City", "Bhadohi");
		
		String cityValue=hmObj.get("City");
		getTestCaseData("test51201AccountCreation");
	}


	public static Workbook getWorkbook(String filePath) throws IOException {
		File fileObj=new File(filePath);
		InputStream is=new FileInputStream(fileObj);

		String[] filePathArr=filePath.split("\\.");
		String extension=filePathArr[1];
		Workbook wbook=null;
		if(extension.equalsIgnoreCase("xlsx")) {
			wbook=new XSSFWorkbook(is);
		}else {
			wbook=new HSSFWorkbook(is);
		}
		return wbook;
	}


	public static int getColumnNumByColumnName(Sheet sheetObj, String columnName) {
		int columnNum=0;
		Row fstRow=sheetObj.getRow(0);
		int columnCount=fstRow.getLastCellNum();
		for(int i=0; i<=columnCount-1; i++) {
			Cell cellObj=fstRow.getCell(i);
			if(cellObj.getStringCellValue().equalsIgnoreCase(columnName)) {
				columnNum=i;
				break;
			}
		}	
		return columnNum;
	}

	public static int getRowNumberByRowId(Sheet sheetObj, String uniqueRowId, String uniqueIdColumnName) {
		int columnNum=getColumnNumByColumnName(sheetObj, uniqueIdColumnName);
		int rowNum=0;
		int rowCount=sheetObj.getLastRowNum();
		for (int i = 0; i <= rowCount; i++) {
			Row rowObj=sheetObj.getRow(i);
			Cell cellObj=rowObj.getCell(columnNum);
			String cellValue=cellObj.getStringCellValue();
			if(cellValue.equalsIgnoreCase(uniqueRowId)) {
				rowNum=i;
				break;
			}

		}
 		return rowNum;				 
	}
	public static String getCellData(Row rowObj, int cellNumber) {
		Cell cellObj=rowObj.getCell(cellNumber, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
		return cellObj.getStringCellValue();

	}

	public static void getTestCaseData(String testCaseName) throws IOException{
		String filePath="TestData.xlsx";
		Workbook wbook=getWorkbook(filePath);		
		Sheet sheetObj=wbook.getSheet("Data");
		int rowNum=getRowNumberByRowId(sheetObj, testCaseName, "TestCaseName");
		int dataStartCoumnNumber=getColumnNumByColumnName(sheetObj, "DataName1");
		Map<String, String>mapObj=new HashMap<String, String>();
		Row dataRowObj=sheetObj.getRow(rowNum);
		int cellCount=dataRowObj.getLastCellNum();
		for(int i=dataStartCoumnNumber; i<=cellCount-1; i=i+2) {
			String dataName=getCellData(dataRowObj, i);
			String dataValue=getCellData(dataRowObj, i+1);
			System.out.println(dataName+"----"+dataValue);
			mapObj.put(dataName, dataValue);
		}
		DataMap=mapObj;

	}

	public static Map<String, String> getPageData(String sheetName, String dataId) throws IOException {
		String dataPath="";
		Workbook wbookObj=getWorkbook(dataPath);
		Sheet pgSheetObj=wbookObj.getSheet(sheetName);
		int dataRowNum=getRowNumberByRowId(pgSheetObj, dataId, "DataID");
		int startDataColumnNumber=getColumnNumByColumnName(pgSheetObj, "DataID")+1;
		Row fstRow=pgSheetObj.getRow(0);
		Row dataRow=pgSheetObj.getRow(dataRowNum);
		int columnCount=fstRow.getLastCellNum();
		Map<String, String> pageDataMap=new HashMap<String, String>();
		for(int i=startDataColumnNumber; i<=columnCount-1;i++) {
			
			String columnName=getCellData(fstRow, i);
			String dataValue=getCellData(dataRow, i);
			pageDataMap.put(columnName, dataValue);
		}
		return pageDataMap;
	}

}
