package automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CssValue {

	static WebDriver dri;

	public static void main(String[] args) {
		dri = Qapractice.launchchrome();
		dri.get("http://localhost:8888/");

	}

//Q57. How do you get the font size of the text box? Or how do you get style property of an element? Ans: Using getCssValue()
	public static void getfontSize(By element) {
		String s = dri.findElement(element).getCssValue("font-size");
		System.out.println(s);
	}

	public static void getfontfamily(By element) {
		String s = dri.findElement(element).getCssValue("font-family");
		System.out.println(s);
	}

	// Q58. Write a script to print background color of a textbox?
	public static void getcolor(By element) {
		String s = dri.findElement(element).getCssValue("color");
		System.out.println(s);
	}

}
