package automation;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HandlingMultipleElements {
	static WebDriver driver;

	@FindBy(how = How.NAME, using = "Login")
	static WebElement login_ck;

	@FindBy(name = "user_name")
	static WebElement userName;

	static By pwd = By.name("user_password");

	public static void main(String[] args) {
		driver = Qapractice.launchchrome();
		driver.get("http://localhost:8888/");
		PageFactory.initElements(driver, JavaScriptinSelenium.class);
		// webelementsMethod();
		webelementsText();

		// Page 57 - Que 66
	}

	// Q65. Write a script to print text of links present on the page.
	public static void webelementsText() {
		List<WebElement> lw = driver.findElements(By.tagName("a"));

		for (WebElement element : lw) {
			String elementText = element.getText();
			System.out.println(elementText);
		}

	}

	public static void webelementsMethod() {
		List<WebElement> lw = driver.findElements(By.tagName("a"));

		int lengthLW = lw.size();
		System.out.println(lengthLW);
		for (int i = 0; i < lengthLW; i++) {
			WebElement getElement = lw.get(i);// get element value by position
			String elementValue = getElement.getText();// get the String or text of element
			System.out.println(i + " : " + elementValue);
		}
	}

}
