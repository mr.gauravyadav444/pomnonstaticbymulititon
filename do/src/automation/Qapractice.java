package automation;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.google.common.io.Files;

/*
i. clear()
ii. click()
iii. getAttribute()
iv. getCssValue()
v. getLocation()
vi. getSize()
vii. getText()
viii. isDisplayed()
ix. isEnabled()
x. isSelected()
xi. sendkeys()
xii. submit()*/

public class Qapractice {
	public static WebDriver driver;
	private static By userName = By.name("user_name");
	private static By pwd = By.cssSelector("input[type='password']");
	private static By login_Button = By.xpath("//input[@name='Login']");
	private static By sign_out = By.xpath("//a[text()='Sign Out']");

//	Write a script to login to vtiger application.
	public static void vtigerLogin() {
		inputValue(userName, "admin");
		inputValue(pwd, "admin");
		elementClick(login_Button);
	}

	public static void openinternetexopengooglebro() {

		System.setProperty("webdriver.ie.driver", "driver/IEDriverServer.exe");
		WebDriver wd = new InternetExplorerDriver();
		wd.get("https://www.google.co.in/");
		System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		wd.close();
	}

	public static void openHtmlfile() {
		driver.get("C:\\Users\\Gaurav\\Desktop\\Prac\\demo.html");
	}

	public static void remotelaunchchrome() {
		System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		// WebDriverManager.chromedriver().setup();
		driver = new RemoteWebDriver(DesiredCapabilities.chrome());

	}

	public static void main(String[] args) {
		launchchrome();
//		resizeBrowser(400, 1000);
//		moveBrowser(400, 400);
		// launchfirefox();

//		openURLto("http://localhost:8888/");
		openURLget("http://localhost:8888/");
		maximizeBrowser();
		// vtigerLogin();
//		backButton();
//		forwardButton();
//		refreshButton();
//		verifyTitle("My Home Page");
		// String url = "http://localhost:8888/index.php?action=index&module=Home";
		// verifyURL(url);
// 		close();
//		openinternetexopengooglebro();
//		openHtmlfile();

		// clickByTagName("a");
		// clickBycssSelector(
		// "a[href='javascript:(function()%7Bvar%20doc=top.document;var%20bodyElement=document.body;doc.vtigerURL%20=%22http://localhost:8888/%22;var%20scriptElement=document.createElement(%22script%22);scriptElement.type=%22text/javascript%22;scriptElement.src=doc.vtigerURL+%22modules/Emails/GmailBookmarkletTrigger.js%22;bodyElement.appendChild(scriptElement);%7D)();']");
		// windowhandels();
		// screenshort();
		// Write an �xpath� which matches with all the links and all the images present
		// on the web page?
		// Ans:- �//a|//img�
		// clickBylinkText("vtiger News");

		// explicitWaitClick("//a[text()='Sign Out']");
		// getAttributevalue("//input[@name='user_name']");
		// removePresentValue("//input[@name='user_name']");
		// removePresentValuewithBackSpace("//input[@name='user_name']");
		// withOutWaitMethod();
		// copyPaste();
		// linkText();
		// coordinatesXandY();
		// sizeElement();
		// alignedhorizontally();
//		sameWidth();
		// HeightVaidation();
//		VerifythefiledEmpty();

//runExeFile("C:/Users/Gaurav/Downloads/geckodriver.exe");
//		inputValue(userName, "Gaurav");
//		CssValue.getfontSize(userName);
//		CssValue.getfontfamily(userName);
//		CssValue.getcolor(userName);
		// accountLandingPage();
// noOfCheckbox();
//		selectCheckboxToptoBottom();
//		URLonPage();
		// tableContent();
		// getsumNumberTable();
		// printNumberInTable(); -----not proper implemented use pattern and matcher
		// multipleselect();
		countOption();
		context();
		// driver.quit();
		// Page85
		// Page90 que106
		// Prac();

	}

	// Q86. How do you handle context menu?
	public static void context() {
		Actions act = new Actions(driver);
		act.contextClick(driver.findElement(login_Button)).build().perform();
	}

	// Q75. Write a script to count no.of options present in the listbox
	// Q76. Write a scrtipt to select all the options present in the listbox and
	// deselect all the option
	// Q77. Write a script to print all the contents of the listbox
	public static void countOption() {
		WebElement we = driver.findElement(By.name("login_theme"));
		Select sl = new Select(we);
		List<WebElement> lw = sl.getOptions();
		int ss = lw.size();
		System.out.println(ss);
		for (int i = 0; i < ss; i++) {
			sl.selectByIndex(i);
			waitThread();
		}
		for (WebElement element : lw) {
			System.out.println(element.getText());
			sl.selectByVisibleText(element.getText());
			waitThread();
		}
	}

	// Q74. Script to verify whether the listbox is single select or mutliselect
	public static void multipleselect() {
		WebElement we = driver.findElement(By.name("login_theme"));
		Select sl = new Select(we);
		boolean st = sl.isMultiple();
		System.out.println(st);
	}

	// Q72. Write a script to print on numbers present in the table without using
	// Try-Catch
	public static void printNumberInTable() {
		int[] numran = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		List<WebElement> lwe_1 = driver.findElements(By.xpath("//td[contains(@onmouseover,'vtlib_listview')]"));
		for (int j = 0; j < lwe_1.size(); j++) {
			String getCellData = lwe_1.get(j).getText();
			System.out.println(getCellData);
			if (getCellData.contains(numran.toString())) {
				System.out.println(getCellData);
			} else {
				System.out.println("else");
			}
		}
		System.out.println();

	}

//Q70. Write a script to print all numbers present in the table.
//	Q71. Write a script to print sum of all the numbers present in the table.
	public static void getsumNumberTable() {
		int sum = 0;
		List<WebElement> lwe_1 = driver.findElements(By.xpath("//td[contains(@onmouseover,'vtlib_listview')]"));
		for (int j = 0; j < lwe_1.size(); j++) {
			String getCellData = lwe_1.get(j).getText();
			try {
				int x = Integer.parseInt(getCellData);
				// System.out.println(x); // prints only numbers
				sum = sum + x;
			} catch (Exception e) {
				System.out.println(getCellData);
			}
		}
		System.out.println();
		System.out.println("Sum is: " + sum);

	}

//	Q69. Write a script to print the contents of the table.
	public static void tableContent() {
		List<WebElement> lwe = driver.findElements(By.xpath("//tr[contains(@id,'row')]"));
		for (int i = 0; i < lwe.size(); i++) {
			String getRowid = lwe.get(i).getAttribute("id");
			List<WebElement> lwe_1 = driver.findElements(
					By.xpath("//td[contains(@onmouseover,'vtlib_listview')]//parent::tr[@id='" + getRowid + "']"));
			for (int j = 0; j < lwe_1.size(); j++) {
				String getcCellData = lwe_1.get(j).getText();
				System.out.print(getcCellData);
			}
			System.out.println(getRowid);
		}
	}

//	Q68. Write a script to print all the url present in link on the page.
	public static void URLonPage() {
//		String val = driver.findElement(By.tagName("a")).getAttribute("href");
//		System.out.println(val + "sdssd");
		List<WebElement> allLinks = driver.findElements(By.xpath("//a"));
		for (WebElement link : allLinks) {
			String url = link.getAttribute("href");
			String text = link.getText();
			if (text.equalsIgnoreCase("")) {

			} else {
				System.out.println(text + "=================================" + url);
			}
		}
	}

//	Q67. Write a script to select all the check boxes present on the page from top to bottom
	public static void selectCheckboxToptoBottom() {
		List<WebElement> lwe = driver.findElements(By.xpath("//input[@type='checkbox'][@name='selected_id']"));
		for (int i = 0; i < lwe.size(); i++) {
			lwe.get(i).click();
			waitThread();
		}

	}

// open account Landing page
	public static void accountLandingPage() {
		WebElement elementSales = driver.findElement(By.xpath("//a[text()='Sales']"));

		Actions act = new Actions(driver);
		act.moveToElement(elementSales).build().perform();
		driver.findElement(By.linkText("Accounts")).click();
	}

//	Q66. Write a script to count the no.of checkboxes present on the page.
	public static void noOfCheckbox() {
		List<WebElement> lwe = driver.findElements(By.xpath("//input[@type='checkbox']"));
		int noofCheckbox = lwe.size();
		System.out.println("No Of Checkbox Present is : " + noofCheckbox);
	}

	// ******************************************************************************************************************************************************************
	// Q56. What are the different ways of clicking on a button?
//	1. click()
//	2. sendKeys()
//	3. submit()//this works only if button code is submit
//	4. javascript
//	5. AutoIt
//	6. Robot Class
	public static void differentClick(By element) {
		driver.findElement(element).click();
		driver.findElement(element).sendKeys(Keys.ENTER);
		driver.findElement(element).submit();
		WebElement element1 = driver.findElement(element);
		JavaScriptinSelenium.jSclick(element1);
	}

	// Q55. Write a script verify whether page is loaded within 3 seconds?

//Q54. Write a script to delete all the cookies present in the browser?
	public static void deleteAllCookies() {
		driver.manage().deleteAllCookies();
	}

//*********************************************************************************************************************************************************************
	// Q53. How do you execute an exe file in Selenium?
	public static void runExeFile(String exePath) {
		try {
			Runtime.getRuntime().exec(exePath);
			System.out.println("Excuted");
		} catch (IOException e) {
			System.out.println("Not Excuted");
		}
	}

	// Q52. Write a script to verify that logo of actitime is displayed on the login
	// page?

	public static void displayedLogo(By element) {
		boolean Stat = driver.findElement(element).isDisplayed();
		if (Stat) {
			System.out.println("Displayed");
		} else {
			System.out.println("Not Displayed");

		}
	}

	// Q51. Write a script to verify whether login button is enabled or not which is
	// present in the FB page?
	public static void buttonIsEnabledOrNot(By element) {
		boolean St = driver.findElement(element).isEnabled();
		if (St == true) {
			System.out.println("isEnabled");
		} else {
			System.out.println("isNotEnabled");

		}
	}

	// Q50. Write a script to verify the status of the check box which is present in
	// FaceBook login page? Note:IsSelected method is used to verify the checkbox or
	// radio button is selected.
	public static void statusCheckBoxAndRadioButton(By element) {
		boolean Status = driver.findElement(element).isSelected();
		if (Status == true) {
			System.out.println("IS Selected");
		} else {
			System.out.println("IS Not Selected");
		}

	}

//	Q49. Write a script to verify that email text box present in Facebook login page is empty?
	// Hint: get the value and check the length of it. Length should be 0.
	public static void VerifythefiledEmpty() {
		WebElement we = driver.findElement(userName);
		we.clear();
		we.sendKeys("a");
		waitThread();

		String text = we.getAttribute("value");
		System.out.println(text.length());

		if (text.length() == 0) {
			System.out.println("Clear");
		} else {
			System.out.println("Not Clear");

		}
	}

	// Q48. Write a script to verify that height of password and Username button
	// which are present in Vtiger login page are same?

	public static void HeightVaidation() {
		WebElement em = driver.findElement(userName);
		Dimension dim = em.getSize();
		int heightem = dim.getHeight();
		WebElement nxt = driver.findElement(pwd);
		Dimension dim_1 = nxt.getSize();
		int heightnxt = dim_1.getHeight();
		System.out.println(heightem + " " + heightnxt);
		if (heightem == heightnxt) {
			System.out.println("Same Height :- " + heightem);

		} else {
			System.out.println("Different Height :- " + heightem + "and" + heightnxt);

		}
	}

//Q47. Write a script to verify that width of userName textbox and pwd textbox is same which are present in Vtiger login page?
	public static void sameWidth() {
		WebElement em = driver.findElement(userName);
		Dimension s1 = em.getSize();
		int w1 = s1.getWidth();
		WebElement nxt = driver.findElement(pwd);
		Dimension s2 = nxt.getSize();
		int w2 = s2.getWidth();
		System.out.println("Width of userName textbox: " + w1);
		System.out.println("Width of pwd textbox: " + w2);
		if (w1 == w2) {
			System.out.println("Width of userName textbox and pwd textbox is same");
		} else {
			System.out.println("Width of userName textbox & pwd textbox is not same");
		}

	}

//	Q46. Write a script to print width and height of a text box?
	public static void sizeElement() {
		Dimension dim = driver.findElement(login_Button).getSize();
		int Height = dim.getHeight();
		int Width = dim.getWidth();
		System.out.println(Height + " " + Width);
	}

	// Q45. Write a script to verify that email text box and Next button present in
	// Gmail login page are aligned horizontally? (x value should be same)
	public static void alignedhorizontally() {
		Point poi_1 = driver.findElement(userName).getLocation();
		int x_1 = poi_1.getX();
		Point poi_2 = driver.findElement(pwd).getLocation();
		int x_2 = poi_2.getX();

		if (x_1 - x_2 <= 0) {
			System.out.println(x_1 + "" + x_2 + "Aligned horizontally");
		} else {
			System.out.println(x_1 + "" + x_2 + "Not Aligned horizontally");

		}
	}

//	Q44. Write a script to print x and y coordinates of an element?
	public static void coordinatesXandY() {
		Point poi = driver.findElement(login_Button).getLocation();
		int x = poi.getX();
		int y = poi.getY();
		System.out.println(x + " " + y);
	}

	// Q43. Write a script to print text of the link?
	public static void linkText() {
		String str = driver.findElement(sign_out).getText();
		System.out.println(str);
	}

//	Q41. Write a script to clear the text present in the text box by pressing back space?
	public static void copyPaste() {
		WebElement v1 = driver.findElement(userName);
		v1.sendKeys("Gaurav");
		waitThread();
		v1.sendKeys(Keys.CONTROL + "a");
		waitThread();
		v1.sendKeys(Keys.CONTROL + "c");
		waitThread();
		WebElement v2 = driver.findElement(pwd);
		waitThread();
		v2.clear();
		waitThread();
		v2.sendKeys(Keys.CONTROL + "v");
	}

	// Q37. Write a script to login and logout from the application without
	// specifying the waiting period or without using any of the Synchronization
	// methods.
	public static void withOutWaitMethod() {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("http://demo.actitime.com");
		driver.findElement(By.id("username")).sendKeys("admin");
		driver.findElement(By.name("pwd")).sendKeys("manager");
		driver.findElement(By.id("loginButton")).click();
		while (true) {
			try {
				driver.findElement(By.id("logoutLink")).click();
				break;
			} catch (NoSuchElementException e) {
				System.out.println("Bye");
			}
		}
	}

	// Q41. Write a script to clear the text present in the text box by pressing
	// back space?
	public static void removePresentValuewithBackSpace(String xpath) {
		WebElement webe = driver.findElement(By.xpath(xpath));
		String attributevalue = webe.getAttribute("value");
		for (int i = 0; i < attributevalue.length(); i++) {
			webe.sendKeys(Keys.BACK_SPACE);

		}
	}

	public static void actionclick(String xpath) {
		WebElement webe = driver.findElement(By.xpath(xpath));
		Actions act = new Actions(driver);
		act.click(webe).build().perform();
	}

	public static void getAttributevalue(String xpath) {
		String attributevalue = driver.findElement(By.xpath(xpath)).getAttribute("value");
		System.out.println(attributevalue);
		driver.findElement(By.xpath(xpath)).sendKeys("value");
		attributevalue = driver.findElement(By.xpath(xpath)).getAttribute("value");
		System.out.println(attributevalue);
	}

	public static void removePresentValue(String xpath) {
		driver.findElement(By.xpath(xpath)).sendKeys(Keys.CONTROL + "a");
		driver.findElement(By.xpath(xpath)).sendKeys(Keys.DELETE);
		String attributevalue = driver.findElement(By.xpath(xpath)).getAttribute("value");
		System.out.println(attributevalue);
	}

	public static void Prac() {
		driver.switchTo().frame("__vtigerBookmarkletFrame__");
		driver.findElement(By.xpath("//input[@id='__searchaccount__']")).sendKeys("Gaurav");
		driver.switchTo().defaultContent();
		WebElement we_action = driver.findElement(By.xpath("//a[text()='Inventory']"));
		Actions act = new Actions(driver);
		act.moveToElement(we_action).build().perform();
	}

	public static void iframeScreenshort() {
		File srcFile = driver.findElement(By.tagName("iframe")).getScreenshotAs(OutputType.FILE);
		File desfile = new File("screenshort/ss.png");
		try {
			Files.copy(srcFile, desfile);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void screenshort() {
		EventFiringWebDriver e = new EventFiringWebDriver(driver);
		File srcFile = e.getScreenshotAs(OutputType.FILE);
		File desFile = new File("screenshort/ss.png");
		try {
			Files.copy(srcFile, desFile);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

// Expilcity Wait it is also called WebdriverWait
	// it will work for the all method
	// after the duration we get timeoutException
	// duration is only seconds
	public static void explicitWaitClick(String xpath) {
//		WebDriverWait wdw = new WebDriverWait(driver, 60);
//		wdw.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)));
//		wdw.until(ExpectedConditions.titleIs("admin"));
		driver.findElement(By.xpath(xpath)).click();
	}

// it will work only for findElement() and findElements() --- it's a Fluent Wait  
// after duration we get noSuchElementException
// duration is in Days, Hours, minutes,seconds
	public static void implicityWait() {
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	}

	public static void pageloadWait() {
		driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
	}

	public static void windowhandels() {
		String s = driver.getWindowHandle();
		System.out.println("getWindowHandle :-" + s);

	}

	// Selenium code to click on a link using �tagName�:
	public static void clickByTagName(String tagName) {
		driver.findElement(By.tagName(tagName)).click();
	}

	// Selenium code to click on a link using �className�:
	public static void clickByclassName(String className) {
		driver.findElement(By.className(className)).click();
	}

	// Selenium code to click on a link using �cssSelector�:
	public static void clickBycssSelector(String cssSelector) {
		driver.findElement(By.cssSelector(cssSelector)).click();
	}

	// Selenium code to click on a link using �id�:
	public static void clickByid(String id) {
		driver.findElement(By.id(id)).click();
	}

	// Selenium code to click on a link using �linkText�:
	// the locator �linkText� can be used only if the element is a link (tag of the
	// element should be a)
	public static void clickBylinkText(String linkText) {
		driver.findElement(By.linkText(linkText)).click();
	}

	// Selenium code to click on a link using �name�:
	public static void clickByName(String name) {
		driver.findElement(By.name(name)).click();
	}

	// Selenium code to click on a link using �partialLinkText�:
	// this locator is used to handle dynamic links.
	public static void clickBypartialLinkText(String partialLinkText) {
		driver.findElement(By.partialLinkText(partialLinkText)).click();
	}

	// Selenium code to click on a link using �xpath�:
	public static void clickByxpath(String xpath) {
		driver.findElement(By.xpath(xpath)).click();
	}

	public static WebDriver launchchrome() {
		System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		// WebDriverManager.chromedriver().setup();

		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		return driver;

	}

	public static void launchfirefox() {
		System.setProperty("webdriver.gecko.driver", "driver/geckodriver.exe");
		driver = new FirefoxDriver();
	}

	public static void close() {
		driver.quit();

	}

	public static void resizeBrowser(int width, int height) {
		Dimension d = new Dimension(width, height);
		driver.manage().window().setSize(d);
	}

	public static void moveBrowser(int x, int y) {
		Point p = new Point(x, y);
		driver.manage().window().setPosition(p);
	}

	public static void maximizeBrowser() {
		driver.manage().window().maximize();
	}

	public static void openURLget(String uRL) {
		driver.get(uRL);
	}

	public static void openURLto(String uRL) {
		driver.navigate().to(uRL);
	}

	public static void backButton() {
		driver.navigate().back();
		waitThread();
	}

	public static void forwardButton() {
		driver.navigate().forward();
		waitThread();
	}

	public static void refreshButton() {
		driver.navigate().refresh();
		waitThread();
	}

	public static void verifyTitle(String acceptedTitle) {
		String actualTitle = driver.getTitle();
		if (actualTitle.contains(acceptedTitle)) {
			System.out.println(
					"Title match : actualTitle is :- " + actualTitle + " and accepted title is :- " + acceptedTitle);

		} else {
			System.out.println("Title not match : actualTitle is :- " + actualTitle + " and accepted title is :- "
					+ acceptedTitle);
		}
	}

	public static void verifyURL(String uRL) {
		String actualURL = driver.getCurrentUrl();
		if (actualURL.contains(uRL)) {
			System.out.println("verified url pass : actualURL:- " + actualURL + " accepted url :- " + uRL);
		} else {
			System.out.println("verified url fail : actualURL:- " + actualURL + " accepted url :- " + uRL);

		}
	}

	public static void waitThread() {
		try {
			Thread.sleep(120);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// find Element will work on first matching locator on web
	// find Element return the webelement
	// if Element is not located then it will provide an exception
	// "NoSuchElementException"
	// clear method will clear the value from the element and return is Void
	// sendkeys Method is use for the input value in string format
	public static void inputValue(By byElement, String Value) {
		WebElement web = driver.findElement(byElement);
		web.clear();
		web.sendKeys(Value);
	}

	public static void elementClick(By byElement) {
		driver.findElement(byElement).click();
	}

}
