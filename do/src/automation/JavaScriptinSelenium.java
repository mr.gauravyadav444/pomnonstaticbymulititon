package automation;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class JavaScriptinSelenium {
	static WebDriver driver;

	@FindBy(how = How.NAME, using = "Login")
	static WebElement login_ck;

	@FindBy(name = "user_name")
	static WebElement userName;

	public static void main(String[] args) {
		driver = Qapractice.launchchrome();
		driver.get("http://localhost:8888/");
		PageFactory.initElements(driver, JavaScriptinSelenium.class);
		driver.findElement(By.name("user_name")).sendKeys("AAAA");
//		jSAlert();
//		driver.switchTo().alert().accept();
//		jSclick(login_ck);
//		waitT();
//		jSSendKeys(userName, "jssendkey");
//		waitT();
//		jSclear(userName);
		elementHighlight(userName);

		// page 51 - q59
	}

// 
	public static void elementHighlight(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// js.executeScript("arguments[0].setAttribute('style', 'border:2px red;')",
		// element);
//		js.executeScript("arguments[0].setAttribute('style', 'background:yellow;border-spacing:2px solid red;')", element);
		js.executeScript("arguments[0].setAttribute('style', 'border: 5px solid #2fa78e;')", element);
	}

	// Javascript Executor Alert
	public static void jSAlert() {
		RemoteWebDriver rwd = (RemoteWebDriver) driver;
		rwd.executeScript("alert('Hi')");
	}

	// Q64. Write a script to scroll to the specific element? Hint: get the Y
	// coordinate of the element using get location method and pass it as argument
	// for �scrollTo� method.
	public static void jSScrollTo(int getY) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0," + getY + ")");
	}

	// Q62. How do you scroll the web page? By using java script(scrollTo)
	// Q63. Write a script to scroll to the bottom of the web page?
	public static void jSScrollToBottom() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// bottom page
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
	}

	public static void jSScrollBottomToTop(int negVal) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// up px
		js.executeScript("window.scrollBy(0,-" + negVal + ")", "");

	}

	public static void jSScrollElementVisible(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// element visible
		js.executeScript("arguments[0].scrollIntoView();", element);

	}

	public static void jSScrollHorizontally(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// horizontally
		js.executeScript("arguments[0].scrollIntoView();", element);

	}

	// Q49. Write a code to remove the value present in the textbox using Java
	// Script?
	public static void jSclear(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('value', '')", element);
	}

	// Q61. How do you enter the value if textbox is disabled? Ans: Using
	// Javascript. ------- Not Try
	//Q60. How do you enter the text into the textbox without using �sendkeys�?
	public static void jSSendKeys(WebElement element, String inputValue) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('value', '" + inputValue + "')", element);
	}

	// Q59. How do you click on the button using Java Script?
	public static void jSclick(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click()", element);
	}

	public static void waitT() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
