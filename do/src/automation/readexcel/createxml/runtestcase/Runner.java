package automation.readexcel.createxml.runtestcase;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class Runner {
	Sheet sheetObj;
	Row rowObj;
	Cell cellObj, cellObjTestcaseID;
	Workbook workbook;
	String cellDataTestcaseID, cellData;
	Set<String> testcaseID = new HashSet<String>();

	@Test
	public Set<String> tain() {
		// Path of the excel file
		FileInputStream fs;

		try {
			fs = new FileInputStream("src/automation/readexcel/createxml/runtestcase/DemoFile.xlsx");
			workbook = new XSSFWorkbook(fs);// --xlsx
//			workbook = new HSSFWorkbook(fs); //-- xlx
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Sheet sheet = workbook.getSheetAt(0);
		sheetObj = workbook.getSheet("Sheet1");

		int lastRow = sheetObj.getLastRowNum();
		for (int i = 0; i <= lastRow; i++) {
			rowObj = sheetObj.getRow(i);
			cellObj = rowObj.getCell(3);
			cellData = cellObj.getStringCellValue();

			if (cellData.equalsIgnoreCase("y")) {
				System.out.println(cellData);
				rowObj = sheetObj.getRow(i);
				cellObjTestcaseID = rowObj.getCell(0);
				cellDataTestcaseID = cellObjTestcaseID.getStringCellValue();
				// System.out.println(cellDataTestcaseID);
				testcaseID.add(cellDataTestcaseID);
			}

		}
		return testcaseID;
	}

	public Set<String> getColoumName(String sheetName) {
		Set<String> cellData = new HashSet<String>();
		sheetObj = workbook.getSheet(sheetName);
		rowObj = sheetObj.getRow(0);
		short lastCellNum = rowObj.getLastCellNum();
		for (int i = 0; i < lastCellNum; i++) {
			cellObj = rowObj.getCell(i);
			String cellDatavalue = cellObj.getStringCellValue();
			cellData.add(cellDatavalue);

		}
		return cellData;
	}

}
