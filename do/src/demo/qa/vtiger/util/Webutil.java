package demo.qa.vtiger.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.google.common.io.Files;

public class Webutil {

	// variable

	private WebDriver driver;
	private WebElement we;

	// driver
	public WebDriver getdriver() {
		return driver;
	}

	// launch Browser
	public void launchBrowser(String KeyName) {
		String name = getPropValue(KeyName);
		if (name.equalsIgnoreCase("ch")) {

			System.setProperty("webdriver.chrome.driver", "dri/chromedriver.exe");
			// WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();

		} else {

			System.out.println("Not Supported Browser");
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(1000, TimeUnit.SECONDS);

	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	public String getORPropFileName(Class<?> object) {
		String orFileName = object.getName();
		orFileName = orFileName.replaceAll("\\.", "/");
		orFileName = orFileName.replaceAll("page", "configlocator") + ".properties";
		return "src/main/java/" + orFileName;
	}

	public Properties loadProperties(String fileName) {
		Properties propObj = new Properties();
		try {
			FileInputStream inputStrem = new FileInputStream(new File(fileName));
			propObj.load(inputStrem);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		setOR(propObj);
		return propObj;
	}

	private Properties orProp;

	public void setOR(Properties propOR) {
		if (this.orProp == null) {
			this.orProp = propOR;
		} else {
			this.orProp = mergePropertiesByUsingPutAll(this.orProp, propOR);
		}
	}

	private Properties mergePropertiesByUsingPutAll(Properties property1, Properties property2) {
		Properties mergedProperties = new Properties();
		mergedProperties.putAll(property1);
		mergedProperties.putAll(property2);
		return mergedProperties;
	}

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	public WebElement findElementConfig(String Keyname) {
		String value = prop.getProperty(Keyname);
		System.out.println(value + " taken Value");
		String[] locName_Locator = value.split("=", 2);
		System.out.println(locName_Locator[0] + " %-=-% " + locName_Locator[1] + " Spilted Value");

		if (locName_Locator[0].equalsIgnoreCase("className")) {
			we = driver.findElement(By.className(locName_Locator[1]));
			System.out.println(locName_Locator[1]);

		} else if (locName_Locator[0].equalsIgnoreCase("cssSelector")) {
			we = driver.findElement(By.cssSelector(locName_Locator[1]));
			System.out.println(locName_Locator[1]);

		} else if (locName_Locator[0].equalsIgnoreCase("id")) {
			we = driver.findElement(By.id(locName_Locator[1]));
			System.out.println(locName_Locator[1]);

		} else if (locName_Locator[0].equalsIgnoreCase("linkText")) {
			we = driver.findElement(By.linkText(locName_Locator[1]));
			System.out.println(locName_Locator[1]);

		} else if (locName_Locator[0].equalsIgnoreCase("name")) {
			we = driver.findElement(By.name(locName_Locator[1]));
			System.out.println(locName_Locator[1]);

		} else if (locName_Locator[0].equalsIgnoreCase("partialLinkText")) {
			we = driver.findElement(By.partialLinkText(locName_Locator[1]));
			System.out.println(locName_Locator[1]);

		} else if (locName_Locator[0].equalsIgnoreCase("tagName")) {
			we = driver.findElement(By.tagName(locName_Locator[1]));
			System.out.println(locName_Locator[1]);

		} else if (locName_Locator[0].equalsIgnoreCase("xpath")) {
			we = driver.findElement(By.xpath(locName_Locator[1]));
			System.out.println(locName_Locator[1]);

		} else {
			System.out.println("Check the config File, Key is :- " + Keyname + " and value is :- " + locName_Locator[0]
					+ " " + locName_Locator[1]);

		}

		return we;
	}

	public void hitURL(String Url) {
		String urlname = getPropValue(Url);
		driver.get(urlname);
	}

	public void close() {
		driver.close();
	}

	private WebElement getElement(By byElement) {
		return driver.findElement(byElement);
	}

	public void sendValue(By byElement, String value) {
		getElement(byElement).clear();
		getElement(byElement).sendKeys(value);
	}

	public void click(By byElement) {
		getElement(byElement).click();
	}

//	 TIME AND DATE
	public String currentTimeDate() {
		String currentDateTime = new Date().toString().replaceAll(":", "_");
		return currentDateTime;
	}

	// configOR
	private Properties prop;

	public void loadConfigOR(String classPathGN, String classNameGSN) {
		// demo.qa.vtiger.page.VtigerLogin
		classPathGN = classPathGN.replace("page", "configlocator");
		// demo.qa.vtiger.configlocator.VtigerLogin

		classPathGN = classPathGN.replace(".", "/");
		// demo/qa/vtiger/configlocator/VtigerLogin
		classPathGN = "src/main/java/" + classPathGN + ".properties";
		// src/main/java/demo/qa/vtiger/configlocator/VtigerLogin.Properties
		config(classPathGN);

	}

	private void config(String confiPath) {
		prop = new Properties();
		try {
			prop.load(new FileInputStream(confiPath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// config file

	public Properties loadPropFile() {
		prop = new Properties();
		try {
			File filePath = new File("src/test/resources/configuration/config.properties");
			FileInputStream fIS = new FileInputStream(filePath);
			prop.load(fIS);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}

	public String getPropValue(String keyvalue) {
		return loadPropFile().getProperty(keyvalue);
	}

	// screenshot

	public String failscreenshot(WebDriver diver, String name) {
		TakesScreenshot takeshot = (TakesScreenshot) driver;
		File srcfile = takeshot.getScreenshotAs(OutputType.FILE);
		String path = "Screenshot/failScreenshot/" + name + " " + currentTimeDate() + ".png";
		File storeFile = new File(path);
		try {
			Files.copy(srcfile, storeFile);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return path;

	}

}