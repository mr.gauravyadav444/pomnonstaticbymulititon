package java;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.testng.annotations.Test;

public class JavaPrac {

	@Test
	public void mm() {
		JavaPrac run = new JavaPrac();
		// run.methodoverriding();
		// run.runtimepolymorphism();
		run.pm();
	}

	public void pm() {
		String str = "d";
		Pattern pattern = Pattern.compile(str);
		String str1 = "GFGFGFGFGFGFGFGFGFG";
		Matcher matcher = pattern.matcher(str1);
		System.out.println(matcher.find());
	}

	public void runtimepolymorphism() {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the name has ff or ch");
		String brname = sc.next();

		webdriver wd;
		if (brname.equals("ch")) {
			wd = new chromedriver();
			wd.webdrivera();
		} else if (brname.equals("ff")) {
			wd = new firefoxdriver();
			wd.webdrivera();
		}
	}

	public void methodoverriding() {
		System.out.println("child refernce variable to child");
		B refvariableb = new B();
		refvariableb.a();
		refvariableb.b();
		refvariableb.c();
		System.out.println("child refernce variable to parent");
		A refvariablebobj = refvariableb;
		refvariablebobj.a();
		refvariablebobj.b();// method overriding between child and parent class
	}
}

// runtime polymorphism
class webdriver {
	void webdrivera() {
		System.out.println("webdriver");
	}
}

class chromedriver extends webdriver {
	void webdrivera() {
		System.out.println("chromedriver");
	}
}

class firefoxdriver extends webdriver {
	void webdrivera() {
		System.out.println("firefoxdriver");
	}
}

// method overriding
class A {
	void a() {
		System.out.println("class A :- a");
	}

	void b() {
		System.out.println("class A :- b");

	}
}

class B extends A {
	void b() {
		System.out.println("class B :- b");

	}

	void c() {
		System.out.println("class B :- c");

	}

}